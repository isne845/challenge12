#include <iostream>
#include <string>
#include <list>

using namespace std;


class Node {
public:
	string name;
	long long id;
	Node* next;
	Node(long long num,string alphabet,Node* ptr = 0) 
	{ 
	  next = ptr;
	  id = num;
	  name = alphabet;
    }
	void display(){
		cout<<"ID: "<<id<<endl;
		cout<<"Name: "<<name<<endl;
		cout<<endl;
	}
};

class List {
public:
	List() { head = tail = 0; }
	~List();
	int isEmpty() { return head == 0; }
	void tailPush(long long num,string alphabet);  
	void searchById(long long);
	void searchByName(string);
	
private:
	Node* head, * tail;
};


List::~List() { 
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::searchById(long long id){
	Node *search = head;
	
	while(search!=NULL){
		if(search->id==id){
			search->display();
			
		}
		search = search->next;
	}
}

void List::searchByName(string name){
	Node *search = head;
	
	
	while(search!=NULL){
		if(search->name==name){
			search->display();
		}
		search = search->next;
	}
}


void List::tailPush(long long num,string alphabet) { 
	Node* t = new Node(num,alphabet);
	if (head==0) {
		head = t;
		tail = t;
	} else {
	tail->next = t;
	tail = t;
	}
	
}

void hashID(List id[]){
	char s_name = 'A';
	
	for(long long s_id = 590611001;s_id<=610615050;s_id++){ //working for 59 to 61
		string name = "";
		name = name+s_name;
		int index = s_id % 50; //hash
		id[index].tailPush(s_id,name); //store data to array 
		s_name++;
		
		if(s_id%50==0){
			s_id = s_id-50;
			s_id = s_id+10000000;
		}
	}
	
	for(long long s_id = 620615001;s_id<=630615050;s_id++){ //working for 62 and 63
		string name = "";
		name = name+s_name;
		int index = s_id % 50; //hash
		id[index].tailPush(s_id,name); //store data to array 
		s_name++;
		
		if(s_id%50==0){
			s_id = s_id-50;
			s_id = s_id+10000000;
		}
	}
}

void hashNAME(List name[]){
	char s_name = 'A';
	
	for(long long s_id = 590611001;s_id<=610615050;s_id++){ //working for 59 to 61
		string na = "";
		na = na+s_name;
		int index = (int)s_name % 50; //hash
		name[index].tailPush(s_id,na); //store data to array 
		s_name++;
		
		if(s_id%50==0){
			s_id = s_id-50;
			s_id = s_id+10000000;
		}
	}
	
	for(long long s_id = 620615001;s_id<=630615050;s_id++){ //working for 62 and 63
		string na = "";
		na = na+s_name;
		int index = (int)s_name % 50; //hash
		name[index].tailPush(s_id,na); //store data to array 
		s_name++;
		
		if(s_id%50==0){
			s_id = s_id-50; 
			s_id = s_id+10000000;
		}
	}
}

void lookupID(List id[]){
	long long input;
    cout<<"\nInput ID: ";
    cin>>input;
    int index = input%50;
	id[index].searchById(input);
	
}

void lookupNAME(List name[]){
	string n="";
	char input;
    cout<<"\nInput Name: ";
    cin>>input;
    int index = (int)input%50;//convert char to int and mod 50
    n = n+input; 
	name[index].searchByName(n);
	
}



int main(){
	
	List id[50];
	List name[50];
	hashID(id);
	hashNAME(name);
	cout<<"MENU TO SEARCH ID AND NAME OF ISNE STUDENT"<<endl;
	int a=1;
		while(a!=0){
			cout<<"1 : look up ID";
			cout<<"\n2 : look up name";
			cout<<"\n0 : exit";
			cout<<"\nchoose menu: ";
			cin>>a;
		
			if(a==1){
				lookupID(id);
			}
			if(a==2){
				lookupNAME(name);
	     	}
		}




	return 0;
}

